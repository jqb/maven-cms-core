
package dept.cms.dao;

import org.hibernate.Session;

import dept.cms.model.User;
import dept.cms.util.HibernateUtil;


/**
 * @author Dong
 *
 */
public class UserDao implements IUserDao{

	/* (non-Javadoc)
	 * @see dept.cms.dao.IUserDao#addUser(dept.cms.model.User)
	 */
	@Override
	public void addUser(User user) {
		Session session = null;
		try {
			session = HibernateUtil.openSession();
			session.beginTransaction();
			session.save(user);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			HibernateUtil.closeSession(session);
		}
	}

	/* (non-Javadoc)
	 * @see dept.cms.dao.IUserDao#getUser(int)
	 */
	@Override
	public User getUser(int id) {
		Session session = null;
		User user = null;
		try {
			session = HibernateUtil.openSession();
			session.beginTransaction();
			user = (User) session.get(User.class, id);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			HibernateUtil.closeSession(session);
		}
		return user;
	}

}
