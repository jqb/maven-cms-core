
package dept.cms.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import dept.cms.model.User;


/**
 * @author Dong
 *
 */
public class TestUserDao {
	private IUserDao userDao;
	
	@Before
	public void setUp() {
		System.out.println();
		System.out.println(" >>> TestUserDao is begining... <<< ");
		userDao = new UserDao();
	}
	@Test
	public void testAddUser() {
		User u = new User("dong", "456"); 
		userDao.addUser(u);
		Assert.assertTrue(u.getId()>0);
		User user = userDao.getUser(1);
		Assert.assertTrue(user.getName().equals("admin"));
	}
	
	@Test
	public void testgetUser() {
		User user = userDao.getUser(1);
		Assert.assertTrue(user.getName().equals("admin"));
	}
	
	@After
	public void tearDown() {
		System.out.println(" >>> TestUserDao is done... <<< ");
		System.out.println();
	}
}
