
package dept.cms.dao;

import dept.cms.model.User;

/**
 * @author Dong
 *
 */
public interface IUserDao {
	public void addUser(User user);
	public User getUser(int id);
}
